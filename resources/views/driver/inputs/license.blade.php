<div class="form-group">
    {{ Form::label('license-number', 'Numer', ['class' => 'form-label']) }}
    {{ Form::text('license[number]', $license->number, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('license-category', 'Kategoria', ['class' => 'form-label']) }}
    {{ Form::select('license[category]', $categories, $license->category, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('license-valid_from', 'Ważne od', ['class' => 'form-label']) }}
    {{ Form::date('license[valid_from]', $license->valid_from, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('license-valid_to', 'Ważne do', ['class' => 'form-label']) }}
    {{ Form::date('license[valid_to]', $license->valid_to, ['class' => 'form-control']) }}
</div>
