@extends('layouts.app')
@section('content')
<div class="container">
    {{ Breadcrumbs::render('driver.create') }}
    {{ Form::open(['route' => 'driver.store']) }}
        <div class="row">
            <div class="col-8">
                <div class="card">
                    <div class="card-body">
                        <h3>Kierowca</h3>
                        @include('driver.inputs.driver', [
                            'driver' => new App\Driver,
                        ])
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <div class="card-body">
                        <h3>Prawo jazdy</h3>
                        @include('driver.inputs.license', [
                            'license' => new App\DriverLicense,
                        ])
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                @include('layouts.form-button', [
                    'text' => 'Dodaj »',
                ])
            </div>
        </div>
    {{ Form::close() }}
</div>
@endsection
