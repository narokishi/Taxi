@extends('layouts.app')
@section('content')
<div class="container">
    {{ Breadcrumbs::render('driver.show', $driver) }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <table class="table table-striped table-hover table-responsive">
                        <tbody>
                            <tr>
                                <th width="1%" scope="row">Imię i nazwisko</th>
                                <td width="2%">{{ $driver->name }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Adres</th>
                                <td>
                                    {{ $driver->address }}<br/>
                                    {{ $driver->postal_code }} {{ $driver->city }} - {{ $driver->country }}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Kontakt</th>
                                <td>
                                    {{ $driver->phone }}<br/>
                                    {{ $driver->email }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-6">
                    <table class="table table-striped table-hover table-responsive">
                        <tbody>
                            <tr>
                                <th width="1%" scope="row">Numer prawa jazdy</th>
                                <td width="2%">{{ $driver->driverLicense->number }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Kategoria</th>
                                <td>{{ $driver->driverLicense->category }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Data ważności</th>
                                <td>
                                    {{ $driver->driverLicense->valid_from }} - {{ $driver->driverLicense->valid_to }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="offset-4 col-4">
                    <a class="btn btn-primary btn-block" href="{{ route('driver.edit', $driver) }}">Edytuj »</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
