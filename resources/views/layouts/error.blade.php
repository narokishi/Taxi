<div class="container text-center">
    @if (isset($errors) && $errors->count())
        <div class="alert alert-danger align-middle">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <ul class="list-unstyled">
                @foreach ($errors->all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>
