<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DriverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge($this->getDriverRules(), $this->getLicenseRules());
    }

    public function messages()
    {
        return [
            '*.required' => 'Wypełnienie pola ":attribute" jest wymagane',
            '*.date'     => 'Pole ":attribute" powinno być datą',
            'before'     => 'Wprowadzone daty są niechronologiczne',
            '*.max'      => 'Pole ":attribute" nie powinno być dłuższe niż :max znaków',
        ];
    }

    public function attributes()
    {
        return [
            'driver.first_name'  => 'Imię',
            'driver.last_name'   => 'Nazwisko',
            'driver.email'       => 'Adres e-mail',
            'driver.phone'       => 'Numer telefonu',
            'driver.address_1'   => 'Ulica',
            'driver.address_2'   => 'Numer domu',
            'driver.address_3'   => 'Numer lokalu',
            'driver.city'        => 'Miasto',
            'driver.postal_code' => 'Kod pocztowy',
            'driver.country'     => 'Państwo',
            'license.number'     => 'Numer',
            'license.valid_from' => 'Ważne od',
            'license.valid_to'   => 'Ważne do',
        ];
    }

    private function getDriverRules()
    {
        return [
            'driver'             => 'required|array',
            'driver.first_name'  => 'required|string|max:25',
            'driver.last_name'   => 'required|string|max:50',
            'driver.email'       => 'required|string|email',
            'driver.phone'       => 'required|string|max:10',
            'driver.address_1'   => 'required|string|max:50',
            'driver.address_2'   => 'required|string|max:10',
            'driver.address_3'   => 'nullable|string|max:10',
            'driver.city'        => 'required|string|max:25',
            'driver.postal_code' => 'required|string|max:10',
            'driver.country'     => 'required|string|max:25',
        ];
    }

    private function getLicenseRules()
    {
        return [
            'license'            => 'required|array',
            'license.number'     => 'required|string|max:20',
            'license.valid_from' => 'required|date|before:license.valid_to',
            'license.valid_to'   => 'required|date',
        ];
    }
}
