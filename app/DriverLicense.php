<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverLicense extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'number',
        'category',
        'valid_from', 'valid_to',
    ];

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }
}
